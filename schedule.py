from datetime import time, datetime, timedelta

from dates_and_time import TimeLine
from dates_and_time import DateTimeRange


class Schedule:

    def __init__(self, weekly_schedule, exceptions_opened=None, exceptions_closed=None):
        if exceptions_closed is None:
            exceptions_closed = []
        if exceptions_opened is None:
            exceptions_opened = []
        self.weekly_schedule = weekly_schedule
        self.exceptions_opened = exceptions_opened
        self.exceptions_closed = exceptions_closed

    class OrdinarySchedule:

        __midnight = time(0, 0, 0)
        __max_day_time = time(23, 59, 59)

        def __init__(self):
            self.__weekdays = [[], [], [], [], [], [], []]

        def add_time_range(self, day, time_range):
            if 0 > day > 6:
                raise ValueError("Expected to input for day from 0-6, Mon-Saturday")

            existing_time_range = self.__weekdays[day]
            if existing_time_range and existing_time_range[-1].end_time >= time_range.start_time:
                raise ValueError("Inputted time range exceeding existed time range frames")

            if time_range.start_time < self.__midnight or time_range.end_time > self.__max_day_time:
                raise ValueError("One day has only 24 hours")

            existing_time_range.append(time_range)

        def add_time_ranges(self, day, *time_ranges):
            for time_range in time_ranges:
                self.add_time_range(day, time_range)

        def __getitem__(self, item):
            return self.__weekdays[item]

    class ExceptionSchedule:
        def __init__(self):
            self.__exception_list = []

        def add_time_range_exceptions(self, time_range):
            if self.__exception_list and self.__exception_list[-1].end_time >= time_range.start_time:
                raise ValueError("Inputted time range exceeding existed time range frames")
            self.__exception_list.append(time_range)

        def add_time_ranges_for_exceptions(self, *time_ranges):
            for time_range in time_ranges:
                self.add_time_range_exceptions(time_range)

        def __iter__(self):
            return iter(self.__exception_list)

    def is_opened(self, timestamp):
        date_time = datetime.fromtimestamp(timestamp)

        for date_time_range in self.exceptions_closed:
            if date_time_range.contains(date_time):
                return False

        for date_time_range in self.exceptions_opened:
            if date_time_range.contains(date_time):
                return True

        for time_range in self.weekly_schedule[date_time.weekday()]:
            if time_range.contains(date_time.time()):
                return True

        return False

    def next_status(self, timestamp):
        date_time = datetime.fromtimestamp(timestamp)

        target_date_time_range = DateTimeRange(date_time, date_time + timedelta(days=7))

        single_timeline = []
        days_range = target_date_time_range.end_time - target_date_time_range.start_time
        for x in range(days_range.days):
            day_time = target_date_time_range.start_time + timedelta(days=x)
            for time_range in self.weekly_schedule[day_time.weekday()]:
                single_timeline.append(time_range.to_date_time_range(day_time).start_time)
                single_timeline.append(time_range.to_date_time_range(day_time).end_time)

        exceptions_time_range_opened = self.create_time_range(target_date_time_range, self.exceptions_opened)

        exceptions_time_range_closed = self.create_time_range(target_date_time_range, self.exceptions_closed)

        merged_time_line = (TimeLine(single_timeline) - TimeLine(exceptions_time_range_closed) +
                            TimeLine(exceptions_time_range_opened)).list_date_time_line_range

        for target_datetime in merged_time_line:
            if target_datetime > target_date_time_range.start_time:
                return target_datetime.timestamp()

        raise ValueError()

    @staticmethod
    def create_time_range(date_range, date_range_list_to_limit):
        exceptions_time_range_opened = []
        for time_range in date_range_list_to_limit:
            if not (date_range.contains(time_range.start_time) or date_range.contains(time_range.end_time)):
                continue
            if time_range.start_time < date_range.start_time:
                a_start = date_range.start_time
            else:
                a_start = time_range.start_time

            if time_range.end_time > date_range.end_time:
                a_end = date_range.end_time
            else:
                a_end = time_range.end_time

            exceptions_time_range_opened.extend([a_start, a_end])
        return exceptions_time_range_opened
