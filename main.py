import argparse
import sys
from datetime import datetime, time
from schedule import Schedule
from dates_and_time import TimeRange, DateTimeRange


class HasToBe(object):
    def __init__(self):
        self.schedule = self.get_schedule()
        parser = argparse.ArgumentParser(
            usage=''' python main.py <command> [<args>]'''
        )
        parser.add_argument("command")
        args =parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print("Unrecognized command")
            exit(1)
        getattr(self, args.command)()


    @staticmethod
    def get_schedule():
        w_h = Schedule.OrdinarySchedule()
        w_h.add_time_ranges(0, TimeRange(time(8), time(12)), TimeRange(time(13), time(17)))
        w_h.add_time_ranges(1, TimeRange(time(8), time(12)), TimeRange(time(13), time(17)))
        w_h.add_time_ranges(2, TimeRange(time(8), time(12)), TimeRange(time(13), time(17)))
        w_h.add_time_ranges(3, TimeRange(time(8), time(13)))
        w_h.add_time_ranges(4, TimeRange(time(8), time(12)), TimeRange(time(13), time(20)))
        w_h.add_time_ranges(5, TimeRange(time(10), time(13)))

        ex_opn = Schedule.ExceptionSchedule()
        ex_opn.add_time_ranges_for_exceptions(DateTimeRange(datetime(2021, 6, 13, 8), datetime(2021, 6, 13, 20)))

        ex_clsd = Schedule.ExceptionSchedule()
        ex_clsd.add_time_ranges_for_exceptions(DateTimeRange(datetime(2021, 5, 1, 0), datetime(2021, 5, 2, 0)),
                                               DateTimeRange(datetime(2021, 5, 20, 9), datetime(2021, 5, 20, 11)),
                                               DateTimeRange(datetime(2021, 6, 1, 6), datetime(2021, 6, 5, 18))
                                               )

        sc = Schedule(w_h, ex_opn, ex_clsd)
        return sc

    def is_opened(self):
        parser = argparse.ArgumentParser(
            description=" returns True if Charging Station`s status is currently \"Opened\" and False if \"Closed\" "
        )
        parser.add_argument("timestamp", type=int)
        args = parser.parse_args(sys.argv[2:])
        print(self.schedule.is_opened(args.timestamp))

    def next_status(self):
        parser = argparse.ArgumentParser(
            description=" Returns next timestamp when the status opened/closed will change"
        )
        parser.add_argument("timestamp", type=int)
        args = parser.parse_args(sys.argv[2:])
        print(self.schedule.next_status(args.timestamp))


if __name__ == '__main__':
    HasToBe()
