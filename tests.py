from datetime import datetime, time

from dates_and_time import TimeRange, DateTimeRange
from schedule import Schedule

w_h = Schedule.OrdinarySchedule()
w_h.add_time_ranges(0, TimeRange(time(8), time(12)), TimeRange(time(13), time(17)))
w_h.add_time_ranges(1, TimeRange(time(8), time(12)), TimeRange(time(13), time(17)))
w_h.add_time_ranges(2, TimeRange(time(8), time(12)), TimeRange(time(13), time(17)))
w_h.add_time_ranges(3, TimeRange(time(8), time(13)))
w_h.add_time_ranges(4, TimeRange(time(8), time(12)), TimeRange(time(13), time(20)))
w_h.add_time_ranges(5, TimeRange(time(10), time(13)))

ex_opn = Schedule.ExceptionSchedule()
ex_opn.add_time_ranges_for_exceptions(DateTimeRange(datetime(2021, 6, 13, 8), datetime(2021, 6, 13, 20)))

ex_clsd = Schedule.ExceptionSchedule()
ex_clsd.add_time_ranges_for_exceptions(DateTimeRange(datetime(2021, 5, 1, 0), datetime(2021, 5, 2, 0)),
                                       DateTimeRange(datetime(2021, 5, 20, 9), datetime(2021, 5, 20, 11)),
                                       DateTimeRange(datetime(2021, 6, 1, 6), datetime(2021, 6, 5, 18))
                                       )

sc = Schedule(w_h, ex_opn, ex_clsd)

assert not sc.is_opened(1615652559)
assert sc.is_opened(datetime(2021, 3, 9, 15).timestamp())

assert sc.next_status(datetime(2021, 3, 9, 3).timestamp()) == datetime(2021, 3, 9, 8).timestamp()
assert sc.next_status(datetime(2021, 3, 9, 9).timestamp()) == datetime(2021, 3, 9, 12).timestamp()
assert sc.next_status(datetime(2021, 6, 13, 20).timestamp()) == datetime(2021, 6, 14, 8).timestamp() # opened exceptoions overwrite

assert sc.next_status(datetime(2021, 6, 1, 5).timestamp()) == datetime(2021, 6, 7, 8).timestamp()  # Closed exceptions overwrite
assert sc.next_status(datetime(2021, 6, 3, 6).timestamp()) == datetime(2021, 6, 7, 8).timestamp()
