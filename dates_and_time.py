from datetime import datetime


class TimeLine:
    def __init__(self, list_date_time_line_range=None):
        if list_date_time_line_range is None:
            list_date_time_line_range = []
        self.list_date_time_line_range = list_date_time_line_range

    def __add__(self, other_timeline):
        other_date_time_range = other_timeline.list_date_time_line_range
        if not other_date_time_range:
            return TimeLine(self.list_date_time_line_range)
        res = []
        if self.list_date_time_line_range[0] < other_date_time_range[0]:
            res.append(self.list_date_time_line_range[0])
        else:
            res.append(other_date_time_range[0])

        if self.list_date_time_line_range[len(self.list_date_time_line_range)-1] > other_date_time_range[1]:
            res.append(self.list_date_time_line_range[len(self.list_date_time_line_range)])
        else:
            res.append(other_date_time_range[1])

        return TimeLine(res)

    def __sub__(self, other_timeline):
        other_date_time_range = other_timeline.list_date_time_line_range
        if not other_date_time_range:
            return TimeLine(self.list_date_time_line_range)
        res = []

        first_on, second_on = False, False

        for date_time in sorted(self.list_date_time_line_range + other_date_time_range):
            if date_time in other_date_time_range:
                second_on = not second_on
                if second_on and first_on or not second_on and first_on:
                    res.append(date_time)
            else:
                first_on = not first_on
                if not second_on:
                    res.append(date_time)

        return TimeLine(res)


class DateTimeRange:
    def __init__(self, start_time, end_time):
        if start_time >= end_time:
            raise ValueError("Start time can not be greater than End time in a time range")

        self.start_time = start_time
        self.end_time = end_time

    def contains(self, time):
        return self.start_time < time < self.end_time

    def __str__(self):
        return "start time {} - end time {}:".format(self.start_time, self.end_time)


class TimeRange:
    def __init__(self, start_time, end_time):
        if start_time >= end_time:
            raise ValueError("Start time can not be greater than End time in a time range")

        self.start_time = start_time
        self.end_time = end_time

    def contains(self, time):
        return self.start_time < time < self.end_time

    def to_date_time_range(self, date) -> DateTimeRange:
        start_time = datetime.combine(date, self.start_time)
        end_time = datetime.combine(date, self.end_time)
        return DateTimeRange(start_time, end_time)

    def __str__(self):
        return "start time {} - end time {}:".format(self.start_time, self.end_time)