class ChargingStation:
    def __init__(self, store, is_employee, charging_station_opened_hours=None, exceptions=None):
        self.store = store
        self.is_employee = is_employee
        self.charging_station_opened_hours = charging_station_opened_hours or store.charging_station_open_hours
        self.exceptions = exceptions or store.exceptions


class Store:
    def __init__(self, name, tenant, charging_stations=None, working_hours=None,
                 exceptions=None, charging_station_open_hours=None):
        if charging_stations is None:
            charging_stations = []
        self.name = name
        self.tenant = tenant
        self.charging_stations = charging_stations
        self.working_hours = working_hours or tenant.working_hours
        self.exceptions = exceptions or tenant.exceptions
        self.charging_station_open_hours = charging_station_open_hours or tenant.charging_station_working_hours


class Tenant:
    def __init__(self, name, working_hours, charging_station_working_hours, exceptions, stores=None):
        if stores is None:
            stores = []
        self.name = name
        self.working_hours = working_hours
        self.charging_station_working_hours = charging_station_working_hours
        self.exceptions = exceptions
        self.stores = stores
